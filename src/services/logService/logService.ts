import fs from 'fs';

export function log(message: string) {
    const time = new Date().toISOString();
    const messageWithTime = `${time} | ${message} \n`;
    console.log(messageWithTime);

    fs.promises.appendFile('logs.csv', messageWithTime);
}
