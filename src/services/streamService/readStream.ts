import * as fs from 'fs';

export async function readStream(pathToFile: string) {
    const readerStream = fs.createReadStream(pathToFile);
    let data = '';

    readerStream.on('data', function (chunk) {
        data += chunk;
    });

    readerStream.on('end', function () {
        console.log(data);
    });
    readerStream.on('error', function (err) {
        console.log(err.stack);
    });

    console.log('Program Ended');
    //readerStream.setEncoding('UTF8');
}
