import * as fs from 'fs';

export function list(dataInPath: string) {
    return fs.promises.readdir(dataInPath);
}
