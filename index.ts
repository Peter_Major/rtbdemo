import path from 'path';
import process from 'process';

import { log } from '@/services/logService/logService';
import { list } from '@/services/fsService/listDirService';
import { readStream } from '@/services/streamService/readStream';

/**
 * @example
 * ```ts
 * const client = new SomeClass();
 * assert(client.doSomething() === 42)
 * ```
 */
async function tmp(): Promise<void> {
    const dir = 'data';
    const subDir = 'data_in';

    const dataInPath = path.join(process.cwd(), dir, subDir);

    const listDir = await list(dataInPath);
    console.log(listDir);
    log(`Started: ${listDir.length} files from ${dataInPath}`);
    for (let item of listDir) {
        log(`Started: ${item}`);
        const dataToFile = path.join(dataInPath, item);
        await readStream(dataToFile);
        log(`Done: ${item}`);
    }
    log(`Done: ${listDir.length} files from ${dataInPath}`);
}

tmp();

//
